<?php

namespace Drupal\iframeremove\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;

/**
 * Provides a filter to remove iframes from displaying according to hostnames.
 *
 * @Filter(
 *   id = "iframeremove_filter",
 *   title = @Translation("iFrame remove filter"),
 *   description = @Translation("Remove iframes from displaying according to hostnames. This is especially useful for sites that allow full HTML inputs."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "iframeremove_whitelist" = "",
 *   }
 * )
 */
class IframeRemoveFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['iframeremove_whitelist'] = [
      '#type' => 'textarea',
      '#title' => t('Whitelist'),
      '#description' => t('Only iframes of these domains are allowed. (One domain per line)'),
      '#default_value' => isset($this->settings['iframeremove_whitelist']) ? $this->settings['iframeremove_whitelist'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Customized security filter to page contents. Removes unsafe iframes from page display.');
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $text = $this->iframeRemoveIframe($text, $this->iframeRemoveMapRegex($this->settings['iframeremove_whitelist']));

    return new FilterProcessResult($text);
  }

  /**
   * Helper function to iframeremove_process_callback().
   *
   * @param string $whitelist
   *   String of domains (with wildcard) to work with.
   *
   * @return array
   *   Array of regular expression to match the domains (with wildcard).
   */
  private function iframeRemoveMapRegex(string $whitelist) {
    $whitelist = trim($whitelist);
    $whitelists = !empty($whitelist) ? preg_split("/([ \t]*(\n|\r\n)+[ \t]*)+/", $whitelist) : [];

    return array_map(function ($host) {
      $host = preg_quote($host, NULL);
      $host = preg_replace('/\\\\\*/', '.*?', $host);
      $host = preg_replace('#/#', '\/', $host);
      return '/' . $host . '/';
    }, $whitelists);
  }

  /**
   * Helper function to iframeremove_whitelist_value_callback().
   *
   * Removes all iframes which host is not within whitelist.
   *
   * @param string $string
   *   HTML content to filter from.
   * @param array $whitelist_regex
   *   Array of regular expressions to match whitelist domains.
   *
   * @return string
   *   HTML with all iframe, except from whitelisted domains, removed.
   */
  private function iframeRemoveIframe(string $string, array $whitelist_regex = []) {
    // Check that there's an iframe in the text somewhere before continuing.
    // Otherwise we'd needlessly be loading every text string into a DOM object
    // when 99% of the time there's no iframes in them.
    if (preg_match('/<\s*iframe /mis', $string)) {
      // Now load the whole text string into a DOM object so we can properly
      // extract the iframe src. Using regex is way too messy and difficult
      // to ensure consistency.
      $dom = Html::load($string);
      $to_remove = [];
      foreach ($dom->getElementsByTagName('iframe') as $iframe) {
        if ($iframe->hasAttribute('src')) {
          $src = $iframe->getAttribute('src');
          $host = parse_url($src, PHP_URL_HOST) . parse_url($src, PHP_URL_PATH);

          // Remove the iframe if it's src is not valid or is not whitelisted.
          if (empty($host) || !$this->iframeRemoveArrayMatch($host, $whitelist_regex)) {
            $to_remove[] = $iframe;
          }
        }
      }

      foreach ($to_remove as $iframe) {
        $iframe->parentNode->removeChild($iframe);
      }

      // If any of the iframes were removed, we need to dump the new DOM to
      // HTML.
      if (count($to_remove) > 0) {
        $html = Html::serialize($dom);
        return $html;
      }
    }

    return $string;
  }

  /**
   * Helper function to _iframeremove_iframe().
   *
   * @param string $host
   *   Domain name from iframe.
   * @param array $regex_list
   *   Array of domain name of whitelist.
   *
   * @return bool
   *   Is matched.
   */
  private function iframeRemoveArrayMatch(string $host, array $regex_list) {
    foreach ($regex_list as $regex) {
      if (preg_match($regex, $host)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
