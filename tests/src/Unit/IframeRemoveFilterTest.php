<?php

namespace Drupal\Tests\iframeremove\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\iframeremove\Plugin\Filter\IframeRemoveFilter;

/**
 * Tests Iframe Remove Filter functions.
 *
 * @coversDefaultClass \Drupal\iframeremove\Plugin\Filter\IframeRemoveFilter
 * @group IframeRemoveFilter
 */
class IframeRemoveFilterTest extends UnitTestCase {

  /**
   * IframeRemoveFilter filter object.
   *
   * @var \Drupal\iframeremove\Plugin\Filter\IframeRemoveFilter
   */
  protected $filter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $configuration['settings'] = [
      'iframeremove_whitelist' => '',
    ];

    $this->filter = new IframeRemoveFilter($configuration, 'filter_html', ['provider' => 'test']);
    $this->filter->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * Test iframe_filter_process_empty().
   *
   * @dataProvider providerIframeRemoveFilterProcessEmpty
   */
  public function testIframeRemoveFilterProcessEmpty($input, $expected) {
    $result = $this->filter->process($input, 'und');

    $this->assertEquals($expected, $result);
  }

  /**
   * Data provider for testIframeRemoveFilterProcessEmpty().
   */
  public static function providerIframeRemoveFilterProcessEmpty() {
    return [
      [
        self::providerMessage(),
        '<div>



</div>',
      ],
    ];
  }

  /**
   * Test iframe_filter_process_wh_drupal().
   *
   * @dataProvider providerIframeRemoveFilterProcessWhDrupal
   */
  public function testIframeRemoveFilterProcessWhDrupal($input, $expected) {
    $configuration['settings'] = [
      'iframeremove_whitelist' => 'www.drupal.org',
    ];

    $this->filter->setConfiguration($configuration);
    $result = $this->filter->process($input, 'und');

    $this->assertEquals($expected, $result);
  }

  /**
   * Data provider for testIframeRemoveFilterProcessDrupal().
   */
  public static function providerIframeRemoveFilterProcessWhDrupal() {
    return [
      [
        self::providerMessage(),
        '<div>

<iframe src="https://www.drupal.org/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>

</div>',
      ],
    ];
  }

  /**
   * Test iframe_filter_process_whj_drupal().
   *
   * @dataProvider providerIframeRemoveFilterProcessWhDrupalMulti
   */
  public function testIframeRemoveFilterProcessWhDrupalMulti($input, $expected) {
    $configuration['settings'] = [
      'iframeremove_whitelist' => '*.drupal.org',
    ];

    $this->filter->setConfiguration($configuration);
    $result = $this->filter->process($input, 'und');

    $this->assertEquals($expected, $result);
  }

  /**
   * Data provider for testIframeRemoveFilterProcessDrupalMulit().
   */
  public static function providerIframeRemoveFilterProcessWhDrupalMulti() {
    return [
      [
        self::providerMessage(),
        '<div>

<iframe src="https://www.drupal.org/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>
<iframe src="https://localize.drupal.org/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>
</div>',
      ],
    ];
  }

  /**
   * Test iframe_filter_process_wh_symfony().
   *
   * @dataProvider providerIframeRemoveFilterProcessWhSymfony
   */
  public function testIframeRemoveFilterProcessWhSymfony($input, $expected) {
    $configuration['settings'] = [
      'iframeremove_whitelist' => 'www.symfony.com',
    ];

    $this->filter->setConfiguration($configuration);
    $result = $this->filter->process($input, 'und');

    $this->assertEquals($expected, $result);
  }

  /**
   * Data provider for testIframeRemoveFilterProcessSymfony().
   */
  public static function providerIframeRemoveFilterProcessWhSymfony() {
    return [
      [
        self::providerMessage(),
        '<div>
<iframe src="https://www.symfony.com/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>


</div>',
      ],
    ];
  }

  /**
   * Provides a string with an example message.
   *
   * @return string
   *   An example message as a string.
   */
  public static function providerMessage() {
    return '<div>
<iframe src="https://www.symfony.com/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>
<iframe src="https://www.drupal.org/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>
<iframe src="https://localize.drupal.org/maps/d/u/0/embed?mid=some-id" width="640" height="480"></iframe>
</div>';
  }

  /**
   * @covers ::setConfiguration
   */
  public function testSetConfiguration() {
    $configuration['settings'] = [
      'iframeremove_whitelist' => '*.drupal.org',
    ];

    $filter = new IframeRemoveFilter($configuration, 'iframeremove_filter', ['provider' => 'test']);
    $this->assertEquals('*.drupal.org', $filter->getConfiguration()['settings']['iframeremove_whitelist']);
  }

}
